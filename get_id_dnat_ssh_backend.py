import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import requests
import json
from pyvcloud.vcd.client import (
    BasicLoginCredentials,
    Client
)
def login(org, user, password,
          api_version="35.0",
          verify_ssl_certs=False,
          log_file="vcd.log",
          log_request=False,
          log_header=False,
          log_body=False):
          client = Client(
              "hn01vcd.fptcloud.com",
              api_version=api_version,
              verify_ssl_certs=verify_ssl_certs,
              log_file=log_file,
              log_requests=log_request,
              log_headers=log_header,
              log_bodies=log_body,
          )
          client.set_credentials(BasicLoginCredentials(user, org, password))
          return client
def get_edge_gateway_nat_rule(client, gateway_id):
    params = RequestInfo(client)

    resp = handle_get(
        f"{params.version_url}/edgeGateways/{gateway_id}/nat/rules", headers=params.headers
    )
    return resp.json()
def handle_get(url, headers):
    print("print url of handle_get: ", url)
    resp = requests.get(url, headers=headers, verify=False)
    if not resp.ok:
        raise Exception(resp.content)
    return resp
class RequestInfo:
    client = None
    headers = None
    xml_headers = None
    api_url = None
    version_url = None
    def __init__(self, client):
        self.version_url = f"{client.get_cloudapi_uri()}/1.0.0"
        self.api_url = client.get_api_uri()
        self.client = client
        self.headers = {
            "Content-Type": "application/json",
            "Accept": "application/json;version=35.0",
            "Authorization": f"Bearer {client._vcloud_access_token}",
        }
        self.xml_headers = {
            "Content-Type": "application/json",
            "Accept": "application/*+xml;version=35.0",
            "Authorization": f"Bearer {client._vcloud_access_token}",
        }
        self.json_special_headers = {
            "Content-Type": "application/json",
            "Accept": "application/*+json;version=35.0",
            # "X-VMWARE-VCLOUD-TENANT-CONTEXT": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            "Authorization": f"Bearer {client._vcloud_access_token}",
        }
def main():
    passwd=r"[m|r>@N$b[rW3qv["
    client = login("000023-xplat", "xplat@fptcloud.com", str(passwd))
    print("print client infor: ", client.__dir__())
    print("print _api_base_uri: ", client._api_base_uri)
    print("print _verify_ssl_certs: ", client._verify_ssl_certs)
    print("print _cloudapi_base_uri: ", client._cloudapi_base_uri)
    print("print get_cloudapi_uri: ", client.get_cloudapi_uri())
    print("print _vcd_api_version: ", client._vcd_api_version)
    print("print _vcloud_access_token: ", client._vcloud_access_token)

    #get_edge_gateway_nat_rule(client, "{{ edge_gateway_id }}")
    get_edge_gateway_nat_rule(client, "urn:vcloud:gateway:8f251440-b3d5-4e73-a6b5-d14a23cf43fe")
    with open('/tmp/id-dnat-ssh-backend.json', 'w') as json_file:
        json.dump(get_edge_gateway_nat_rule(client, "urn:vcloud:gateway:8f251440-b3d5-4e73-a6b5-d14a23cf43fe"), json_file)

if __name__ == "__main__":
    main()
