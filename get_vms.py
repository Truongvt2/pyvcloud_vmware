import sys
from pyvcloud.vcd.client import BasicLoginCredentials
from pyvcloud.vcd.client import Client
from pyvcloud.vcd.org import Org
from pyvcloud.vcd.vdc import VDC
from pyvcloud.vcd.vapp import VApp
from pyvcloud.vcd.vm import VM
import requests

host = "https://hn01vcd.fptcloud.com"
user = "xplat@fptcloud.com"
password = "[m|r>@N$b[rW3qv["
org = "000023-xplat"
vdc_name = "XPLAT-VPC"
check_target_vm = "backend"

# Disable warnings from self-signed certificates.
requests.packages.urllib3.disable_warnings()

# Login. SSL certificate verification is turned off to allow self-signed
# certificates.  You should only do this in trusted environments.
print("Logging in: host={0}, org={1}, user={2}".format(host, org, user))
client = Client(host, verify_ssl_certs=False)
client.set_highest_supported_version()
client.set_credentials(BasicLoginCredentials(user, org, password))
orgs = client.get_org_list()
def check_state(check_target_vm):
    for org in orgs:
        orgObject = Org(client, href=org.attrib["href"])
        orgName = org.attrib["name"]
        for vdc_info in orgObject.list_vdcs():
            vdcName = vdc_info['name']
            if vdcName == vdc_name:
                vdcHref = vdc_info['href']
                vdc = VDC(client, href=vdcHref)
                for resource in vdc.list_resources():
                    if resource["type"] == "application/vnd.vmware.vcloud.vApp+xml":
                        currentVappHref = vdc.get_vapp_href(resource["name"])
                        currentVapp = VApp(client, href=currentVappHref)
                        vmList = currentVapp.get_all_vms()                    
                        for vmElement in vmList:                        
                            vmName = vmElement.attrib["name"]      
                            if vmName == check_target_vm:
                                return True
    return False
                    
result = check_state(check_target_vm)
print(result)
                    # vm = VM(client, resource=vmElement)  # no needed in my case 
#                    print(f"{orgName=} {vdcName=} {vmName=}")  # Python >= 3.8

# Log out.
#print("Logging out")
#client.logout()```

#for vm in vapp.get_all_vms():
#    print(vm.get('name'))
