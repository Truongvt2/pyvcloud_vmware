from module_utils.login import login
from module_utils.vdc import VDC
from module_utils.vapp import VApp
from module_utils.client import Client
import json
import collections
from lxml import etree
from lxml import objectify
from pyvcloud.vcd.client import RelationType
from pyvcloud.vcd.client import ResourceType
from pyvcloud.vcd.client import EntityType
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

vCloud_hostname = 'https://han.fptcloud.com'
org_name = 'XPLAT-HAN-ORG'
vCloud_vdc = 'XPLAT-HAN-DB-VDC'
vCloud_username = 'truongvv10'
vCloud_pwd = 'okd@123'


vapp_name = "truongvv10psql-123"

# vapp_name = "backend-xplat-han-db-vdc"
class Vm_detail(object):
    client = login(vCloud_hostname, org_name, vCloud_vdc, vCloud_username, vCloud_pwd)
    vapp = client.vdc.get_vapp(vapp_name)
    vapp_resource = VApp(client, resource=vapp)
    vm_resource = vapp_resource.resource.Children.Vm
    def __init__(self, vm_name):
        self.vm_detail = self.vm_parameter()
        self.vm_metadata = self.vm_metadata()
    def vm_parameter(self):
        vm_list_parameter = []
        for vm in self.vm_resource:
            vm_list = collections.OrderedDict()
            vm_list["ComputerName"] = str(vm.GuestCustomizationSection.ComputerName)
            vm_list["Ip_address"] = str(vm.NetworkConnectionSection.NetworkConnection.IpAddress)
            vm_list["OsType"] = str(vm.VmSpecSection.OsType)
            vm_list["Numbercore"] = str(vm.VmSpecSection.NumCpus)
            vm_list["Core_per_socket"] = str(vm.VmSpecSection.NumCoresPerSocket)
            vm_list["Memory_MB"] = str(vm.VmSpecSection.MemoryResourceMb.Configured)
            vm_list["Disk_MB"] = str(vm.VmSpecSection.DiskSection.DiskSettings.SizeMb)
            vm_list_parameter.append(vm_list)
        return json.dumps(vm_list_parameter)

    def vm_metadata(self):
        try:
            vm_list_metadata_all = []
            print("count number vms:", len(self.vm_resource))
            for vm in self.vm_resource:
                vm_resource = vm
                # vm_name = str(vm.GuestCustomizationSection.ComputerName)
                a = self.client.client.get_linked_resource(vm_resource, RelationType.DOWN, EntityType.METADATA.value).MetadataEntry
                vm_list_metadata = []
                for i in a:
                    vm_metadata = collections.OrderedDict()
                    vm_metadata["key"] = str(i.Key)
                    vm_metadata["value"] = str(i.TypedValue.Value)
                    vm_list_metadata.append(vm_metadata)
                a = json.dumps(vm_list_metadata)
                vm_list_metadata_all.append(a)
                # vm_list_metadata_all.append(output_dict)
            return vm_list_metadata_all
            # vm_list_metadata_all = vm_list_metadata_all.append(a)
            # return vm_list_metadata_all
        except Exception as err:
            print(f"Error encountered in get_vapp(), Error: {err}")
            return False

VM = Vm_detail(vapp_name)
# print(VM)
print("vm_resource_infor: ", VM.vm_detail)

for i in range(len(VM.vm_metadata)):
    print("vm_metadata", VM.vm_metadata[i])
