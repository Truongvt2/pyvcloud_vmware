import logging
from pathlib import Path
import logging.handlers as handlers
import requests
import sys
import time
import urllib
from lxml import objectify
from enum import Enum
from pyvcloud.vcd.vcd_api_version import VCDApiVersion

ALPHA_API_SUBSTRING = "alpha"

class ApiVersion(Enum):
    VERSION_29 = '29.0'
    VERSION_30 = '30.0'
    VERSION_31 = '31.0'
    VERSION_32 = '32.0'
    VERSION_33 = '33.0'
    VERSION_34 = '34.0'
    VERSION_35 = '35.0'
    VERSION_36 = '36.0'
    VERSION_37_ALPHA = '37.0.0-alpha'


class VcdApiVersionObj(Enum):
    VERSION_29 = VCDApiVersion(ApiVersion.VERSION_29.value)
    VERSION_30 = VCDApiVersion(ApiVersion.VERSION_30.value)
    VERSION_31 = VCDApiVersion(ApiVersion.VERSION_31.value)
    VERSION_32 = VCDApiVersion(ApiVersion.VERSION_32.value)
    VERSION_33 = VCDApiVersion(ApiVersion.VERSION_33.value)
    VERSION_34 = VCDApiVersion(ApiVersion.VERSION_34.value)
    VERSION_35 = VCDApiVersion(ApiVersion.VERSION_35.value)
    VERSION_36 = VCDApiVersion(ApiVersion.VERSION_36.value)
    VERSION_37_ALPHA = VCDApiVersion(ApiVersion.VERSION_37_ALPHA.value)

API_CURRENT_VERSIONS = [
    ApiVersion.VERSION_29.value,
    ApiVersion.VERSION_30.value,
    ApiVersion.VERSION_31.value,
    ApiVersion.VERSION_32.value,
    ApiVersion.VERSION_33.value,
    ApiVersion.VERSION_34.value,
    ApiVersion.VERSION_35.value,
    ApiVersion.VERSION_36.value
]

VCD_API_CURRENT_VERSIONS = [
    VcdApiVersionObj.VERSION_29.value,
    VcdApiVersionObj.VERSION_30.value,
    VcdApiVersionObj.VERSION_31.value,
    VcdApiVersionObj.VERSION_32.value,
    VcdApiVersionObj.VERSION_33.value,
    VcdApiVersionObj.VERSION_34.value,
    VcdApiVersionObj.VERSION_35.value,
    VcdApiVersionObj.VERSION_36.value,
    VcdApiVersionObj.VERSION_37_ALPHA.value
]





uri = 'https://hn01vcd.fptcloud.com'


class Client(object):

    """A low-level interface to the vCloud Director REST API.

    Clients default to the production vCD API version as of the pyvcloud
    module release and will try to negotiate down to a lower API version
    that pyvcloud certifies if the vCD server is older. You can also set
    the version explicitly using the api_version parameter or by calling
    the set_highest_supported_version() method.

    The log_file is set by the first client instantiated and will be
    ignored in later clients.

    :param str uri: vCD server host name or connection URI.
    :param str api_version: vCD API version to use.
    :param boolean verify_ssl_certs: If True validate server certificate;
        False allows self-signed certificates.
    :param str log_file: log file name or None, which suppresses logging.
    :param boolean log_request: if True log HTTP requests.
    :param boolean log_headers: if True log HTTP headers.
    :param boolean log_bodies: if True log HTTP bodies.
    """

    _HEADER_ACCEPT_NAME = 'Accept'
    _HEADER_AUTHORIZATION_NAME = 'Authorization'
    _HEADER_CONNECTION_NAME = 'Connection'
    _HEADER_CONTENT_LENGTH_NAME = 'Content-Length'
    _HEADER_CONTENT_RANGE_NAME = 'Content-Range'
    _HEADER_CONTENT_TYPE_NAME = 'Content-Type'
    _HEADER_REQUEST_ID_NAME = 'X-VMWARE-VCLOUD-REQUEST-ID'
    _HEADER_X_VCLOUD_AUTH_NAME = 'x-vcloud-authorization'
    _HEADER_X_VMWARE_CLOUD_ACCESS_TOKEN_NAME = 'x-vmware-vcloud-access-token'
    _HEADER_CONNECTION_VALUE_CLOSE = 'close'
    _HEADERS_TO_REDACT = [
        _HEADER_AUTHORIZATION_NAME,
        _HEADER_X_VCLOUD_AUTH_NAME,
        _HEADER_X_VMWARE_CLOUD_ACCESS_TOKEN_NAME
    ]

    _UPLOAD_FRAGMENT_MAX_RETRIES = 5


    def __init__(self,
                 uri,
                 api_version=None,
                 verify_ssl_certs=True,
                 log_file=None,
                 log_requests=False,
                 log_headers=False,
                 log_bodies=False):
        self._logger = None
        self._get_default_logger(file_name=log_file)

        self._log_requests = log_requests
        self._log_headers = log_headers
        self._log_bodies = log_bodies
        self._verify_ssl_certs = verify_ssl_certs

        self.fsencoding = sys.getfilesystemencoding()

        self._api_base_uri = self._prep_base_uri(uri)
        self._cloudapi_base_uri = self._prep_base_uri(uri, True)
        self._api_version = api_version
        self._vcd_api_version = None
        if api_version:
            self._vcd_api_version = VCDApiVersion(api_version)

        self._session_endpoints = None
        self._session = None
        self._vcloud_session = None
        self._vcloud_auth_token = None
        self._vcloud_access_token = None
        self._query_list_map = None
        self._task_monitor = None

        self._is_sysadmin = False


    def _get_default_logger(self, file_name="vcd_pysdk.log",
                        log_level=logging.DEBUG,
                        max_bytes=30000000, backup_count=30):
        if file_name is None:
            file_name = "vcd_pysdk.log"
        self._logger = logging.getLogger(file_name)
        self._logger.setLevel(log_level)
        file = Path(file_name)
        if not file.exists():
            file.parent.mkdir(parents=True, exist_ok=True)
        if not self._logger.handlers:
            log_handler = handlers.RotatingFileHandler(
                filename=file_name, maxBytes=max_bytes,
                backupCount=backup_count)
            formatter = logging.Formatter(
                fmt='%(asctime)s | %(module)s:%(lineno)s - %(funcName)s '
                    '| %(levelname)s :: %(message)s',
                datefmt='%y-%m-%d %H:%M:%S')
            log_handler.setFormatter(formatter)
            log_handler.setLevel(log_level)
            self._logger.addHandler(log_handler)

    def _prep_base_uri(self, uri, is_cloudapi=False):
        result = uri
        if len(result) > 0:
            if result[-1] != '/':
                result += '/'

            if is_cloudapi:
                result += 'cloudapi'
            else:
                result += 'api'

            if not result.startswith('https://') and not result.startswith('http://'):  # noqa: E501
                result = 'https://' + result
        return result

    def get_supported_versions_list(self, include_alpha_versions: bool = False):  # noqa: E501
        """Return non-deprecated server API versions as a list.

        :param bool include_alpha_versions: boolean indicating if alpha
            versions should be included in the result.

        :return: versions as strings, sorted in numerical order.

        :rtype: list
        """
        with requests.Session() as new_session:
            # Use with block to avoid leaking socket connections.
            response = self._do_request_prim(
                'GET',
                self._api_base_uri + '/versions',
                new_session)
            if response.status_code != requests.codes.ok:
                raise VcdException('Unable to get supported API versions.')

            versions = objectify.fromstring(response.content)
            active_versions = []
            for version in versions.VersionInfo:
                # Versions must be explicitly assigned as text values using the
                # .text property. Otherwise lxml will return "corrected"
                # numbers that drop non-significant digits. For example, 5.10
                # becomes 5.1.  This transformation corrupts the version.

                if not hasattr(version, 'deprecated') or \
                        version.get('deprecated').lower() == 'false':
                    active_versions.append(str(version.Version.text))
            if include_alpha_versions and hasattr(versions, "AlphaVersion"):
                for version in versions.AlphaVersion:
                    if not hasattr(version, 'deprecated') or \
                            version.get('deprecated') == 'false':
                        # alpha version may be of the form `3X.0.0-alpha-12345`
                        # so we remove the portion after "alpha"
                        alpha_version = str(version.Version.text)
                        start_alpha_ind = alpha_version.find(ALPHA_API_SUBSTRING)  # noqa: E501
                        if start_alpha_ind != -1:
                            alpha_version = alpha_version[:start_alpha_ind + len(ALPHA_API_SUBSTRING)]  # noqa: E501
                        active_versions.append(alpha_version)
            active_versions.sort(key=VCDApiVersion)
            return active_versions

    def _do_request_prim(self,
                         method,
                         uri,
                         session,
                         contents=None,
                         media_type=None,
                         accept_type=None,
                         auth=None,
                         params=None,
                         extra_headers=None):
        headers = extra_headers or {}
        if media_type is not None:
            headers[self._HEADER_CONTENT_TYPE_NAME] = media_type

        if not accept_type:
            accept_header = 'application/*+xml'
        else:
            accept_header = accept_type
        if self._api_version:
            accept_header += f";version={self._api_version}"
        headers[self._HEADER_ACCEPT_NAME] = accept_header

        if contents is None:
            data = None
        else:
            if isinstance(contents, dict):
                data = json.dumps(contents)
            else:
                data = etree.tostring(contents)

        self._log_request_sent(
            method=method, uri=uri, headers=headers, request_body=data)

        response = session.request(
            method,
            uri,
            params=params,
            data=data,
            headers=headers,
            auth=auth,
            verify=self._verify_ssl_certs)

        self._log_request_response(response=response)

        return response

    def get_api_uri(self):
        """Retrieve the uri for vCD api endpoint.

        :return: base api uri

        :rtype: str
        """
        return self._api_base_uri


    def _log_request_sent(self, method, uri, headers={}, request_body=None):
        if not self._log_requests:
            return

        self._logger.debug(f"Request uri {method}: {uri}")

        if self._log_headers:
            self._logger.debug(f"Request partial headers: {self._redact_headers(headers)}")  # noqa: E501

        if self._log_bodies and request_body is not None:
            if isinstance(request_body, str):
                body = request_body
            else:
                body = request_body.decode(self.fsencoding)
            self._logger.debug('Request body: %s' % body)

    def _log_request_response(self,
                              response,
                              skip_logging_response_body=False):
        if not self._log_requests:
            return

        if self._log_headers:
            self._logger.debug(f"Request full headers: {self._redact_headers(response.request.headers)}")  # noqa: E501

        self._logger.debug('Response status code: %s' % response.status_code)

        if self._log_headers:
            self._logger.debug('Response headers: %s' % self._redact_headers(
                response.headers))

        if self._log_bodies and not skip_logging_response_body and \
                _response_has_content(response):
            if isinstance(response.content, str):
                response_body = response.content
            else:
                response_body = response.content.decode(self.fsencoding)
            self._logger.debug('Response body: %s' % response_body)

    def set_highest_supported_version(self):
        """Set the client API version to the highest server API version.

        This call is intended to make it easy to work with new vCD
        features before they are officially supported in pyvcloud.
        Production applications should either use the default pyvcloud API
        version or set the API version explicitly to freeze compatibility.

        :return: selected api version.

        :rtype: str
        """
        active_versions = self.get_supported_versions_list()
        self._api_version = active_versions[-1]
        self._vcd_api_version = VCDApiVersion(self._api_version)
        self._logger.debug('API versions supported: %s' % active_versions)
        self._logger.debug('API version set to: %s' % self._api_version)
        return self._api_version

#print base_uri
a = Client(uri)
b= a._prep_base_uri(uri)
print("base_uri:", b)
c= a.get_supported_versions_list(uri)
print("print list version: ", c)
d = a.set_highest_supported_version()
print("highest supported version: ", d)