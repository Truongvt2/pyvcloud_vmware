import requests

header  = {
        'Accept': 'application/*+xml;version=36.3',
        'Accept-Language': 'en-US,en;q=0.9',
        'Authorization': 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ4cGxhdEBmcHRjbG91ZC5jb20iLCJpc3MiOiJhMWVhNjViNy1hOTJlLTQ2YzgtYWFjNi1lNzM4Mjg1MDZkYzdAODdhOTRiMGYtZTg5Zi00ZDcyLWEwMjktZjBiZmYzMGRmYzE2IiwiZXhwIjoxNjYyNDM1NjI2LCJ2ZXJzaW9uIjoidmNsb3VkXzEuMCIsImp0aSI6ImE5ZmE0ZmI0NGZmMDRjMzJhNGY2ZjU2ZTlhNTg5N2M4In0.BBpkyWm2R9wppSbXKMVocYQUB9D3Kc5_c6FEKGUir2Juxo5u8SO5bg6Ge4PYiZU1ztPJFLaLVdQ3DIA6ABb5ErUkBqZG8pqmrwHYhwvLRkMTd1WdmLuyh0JpizAjxoY3_80xf1l_jNdEdJZhVfDNKUEUzXHoM5PPjNffDxjT37HK5c0IMWiNkwE0SxWwV2rmro0eXqd6jSp5tBsNtFBOMdJrzdyJEla_mGcMtF7Au6e29dLUqmVp_QtFtnp6rAv5_UywZRG9coHuZhTr_8Dzsy9cwpuxEGo0RpivILoxyxcIzSQzfG8slJOsWQWc5l839KI-tsUwo2EoYQYKHWdLTA'
,
        'Connection': 'keep-alive',
        'Content-Type': 'application/*+xml;charset=UTF-8',
        'Origin': 'https://hn01vcd.fptcloud.com',
        'Referer': 'https://hn01vcd.fptcloud.com/tenant/000023-xplat/vdcs/5ea37321-0d25-4617-8fbf-b580fa94fcc5/affinityRules',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'X-VMWARE-VCLOUD-AUTH-CONTEXT': '000023-xplat',
        'X-VMWARE-VCLOUD-TENANT-CONTEXT': 'a1ea65b7-a92e-46c8-aac6-e73828506dc7',
        'X-vCloud-Authorization': '000023-xplat'
        }