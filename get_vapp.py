import sys
from pyvcloud.vcd.client import BasicLoginCredentials
from pyvcloud.vcd.client import Client
from pyvcloud.vcd.org import Org
from pyvcloud.vcd.vdc import VDC
from module_utils.vapp import VApp
from pyvcloud.vcd.vm import VM
import requests
import json

host = "https://hn01vcd.fptcloud.com"
user = "xplat@fptcloud.com"
password = "[m|r>@N$b[rW3qv["
org = "000023-xplat"
vdc_name = "XPLAT-VPC"
target_vapp = "backend-xplat-vpc"

# Disable warnings from self-signed certificates.
requests.packages.urllib3.disable_warnings()

# Login. SSL certificate verification is turned off to allow self-signed
# certificates.  You should only do this in trusted environments.
print("Logging in: host={0}, org={1}, user={2}".format(host, org, user))
client = Client(host, verify_ssl_certs=False)
client.set_highest_supported_version()
client.set_credentials(BasicLoginCredentials(user, org, password))
orgs = client.get_org_list()

vm = 'truongvv10psql6-1234-master1'
debug = client.vapp.get_vm(vm)


def check_state(target_vapp):
    for org in orgs:
        orgObject = Org(client, href=org.attrib["href"])
        orgName = org.attrib["name"]
        for vdc_info in orgObject.list_vdcs():
            vdcName = vdc_info['name']
            if vdcName == vdc_name:
                vdcHref = vdc_info['href']
                vdc = VDC(client, href=vdcHref)
                list_all = vdc.list_resources()
                for resource in vdc.list_resources():
                    if resource["name"] == target_vapp:
                        return True
    return False
                    
result = check_state(target_vapp)
print(result)
