from module_utils.client import Client, BasicLoginCredentials
from module_utils.org import Org
from module_utils.vdc import VDC
from module_utils.vm import VM
from pyvcloud.vcd.client import RelationType
from pyvcloud.vcd.client import ResourceType
from pyvcloud.vcd.client import EntityType
from lxml import etree
from lxml import objectify
from pyvcloud.vcd.exceptions import EntityNotFoundException
from module_utils.vapp import VApp
import collections
import json

class VCD_Utils():
    def __init__(self, vcloud_host_name, vcloud_org_name,
                 vcloud_org_vdc_name,
                 vcloud_user_name, vcloud_user_pwd):
        try:
            self.client = ""
            self.vdc = ""
            self.org = ""
            self.client = Client(vcloud_host_name,
                                 verify_ssl_certs=False,
                                 log_requests=False,
                                 log_headers=False,
                                 log_bodies=False)
            self.client.set_credentials(BasicLoginCredentials(vcloud_user_name,
                                                              vcloud_org_name,
                                                              vcloud_user_pwd))
            self.org_resource = self.client.get_org()
            self.org = Org(self.client, resource=self.org_resource)
            self.vdc_resource = self.org.get_vdc(name=vcloud_org_vdc_name)
            self.vdc = VDC(self.client, resource=self.vdc_resource)
        except Exception as err:
            print(f"Error encountered in login(), Error: {err}")

    def get_vapp(self, vapp_name):
        try:
            vapp_resource = self.vdc.get_vapp(vapp_name)
            vapp = VApp(self.client, resource=vapp_resource)
            print("debug vapp: ", vapp)
            return vapp
        except Exception as err:
            print(f"Error encountered in get_vapp(), Error: {err}")
            return False


    def get_metadata(self, vapp_name):
        try:
            vapp_resource = self.vdc.get_vapp(vapp_name)
            return self.client.get_linked_resource(vapp_resource, RelationType.DOWN, EntityType.METADATA.value)
        except Exception as err:
            print(f"Error encountered in get_vapp(), Error: {err}")
            return False
    def get_metadata_vm(self, vapp_name):
        try:
            output = self.get_vapp(vapp_name)
            vm_infor = output.resource.Children.Vm
            vm_list_metadata = []
            for vm in vm_infor:
                vm_resource = vm
                a = self.client.get_linked_resource(vm_resource, RelationType.DOWN, EntityType.METADATA.value).MetadataEntry
                for i in a:
                    vm_metadata = collections.OrderedDict()
                    vm_metadata["key"] = str(i.Key)
                    vm_metadata["value"] = str(i.TypedValue.Value)
                    vm_list_metadata.append(vm_metadata)
            return json.dumps(vm_list_metadata)
        except Exception as err:
            print(f"Error encountered in get_vapp(), Error: {err}")
            return False